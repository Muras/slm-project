package com.org.Api;

public class ApiResults<T> {

    private String msg;
    private boolean status;
    private T data;

    public ApiResults(String msg, boolean status) {
        this.msg = msg;
        this.status = status;
    }

    public ApiResults(String msg, boolean status, T data) {
        this.msg = msg;
        this.status = status;
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
