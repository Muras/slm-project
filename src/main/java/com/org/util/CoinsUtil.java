package com.org.util;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;

@Component
public class CoinsUtil {

    private static RestTemplate restTemplate =new RestTemplate();
    private static Environment environment;

    public CoinsUtil(Environment environment) {
        this.environment = environment;
    }



    public static HashMap generateWallet(String api_key){

         String passUrl = environment.getProperty("block.io");



        api_key = environment.getProperty(api_key);

        passUrl = passUrl.concat("get_new_address?api_key=").concat(api_key);

        HashMap result = restTemplate.postForObject(passUrl,new HashMap<>(), HashMap.class);
        return result;
    }

    public static HashMap checkBal(String api_key, String address){

        String passUrl = environment.getProperty("block.io");
        api_key = environment.getProperty(api_key);

        passUrl = passUrl.concat("get_balance?api_key=").concat(api_key).concat("&addresses=").concat(address);;

        HashMap result = restTemplate.postForObject(passUrl,new HashMap<>(), HashMap.class);


        return result;
    }

    public static HashMap withdrawBal(String api_key, String address,String toAddress,String amount){

        String passUrl = environment.getProperty("block.io");
        api_key = environment.getProperty(api_key);


        passUrl = passUrl.concat("withdraw_from_addresses/?api_key=")
                .concat(api_key)
                .concat("&from_addresses=").concat(address)
                .concat("&to_addresses=").concat(toAddress)
                .concat("&amounts=").concat(new BigDecimal(amount).toPlainString())
                .concat("&pin=").concat(environment.getProperty("pin"));
        System.out.println(passUrl);
        HashMap result = restTemplate.postForObject(passUrl,new HashMap<>(), HashMap.class);
        return result;
    }


}

