package com.org.service;

import com.org.model.User;

import java.util.List;

public interface UserService {

    void addUser(User user);
    List<User> allUser();
    User logIn(String username);
    boolean checkExistUsername(String username);
    boolean checkExistemail(String email);
    User getUserById(Long id);
    User getUserByusername(String username);


}
