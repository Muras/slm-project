package com.org.service;

import com.org.model.User;
import com.org.repository.UserRepository;
import com.org.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Component
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void addUser(User user) {
        userRepository.save(user);
    }

    @Override
    public List<User> allUser() {
        //System.out.println(userRepository.findAll());
       List<User> userlist = userRepository.findAll();
       return userlist;
    }

    @Override
    public User logIn(String username) {

        return userRepository.findByusername(username);
    }

    @Override
    public boolean checkExistUsername(String username) {
       return userRepository.checkExistsUsername(username);
    }

    @Override
    public boolean checkExistemail(String email) {
        return userRepository.checkExistsemail(email);
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findByid(id);
    }

    @Override
    public User getUserByusername(String username) {
        return userRepository.findByusername(username);
    }


}
