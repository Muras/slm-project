package com.org.controller;

import com.org.Api.ApiResults;
import com.org.model.Token;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by thomson on 5/10/17.
 */

@RestController
@CrossOrigin
@RequestMapping(value = "token")
public class TokenController {


    private Token token;

    public TokenController(Token token) {
        this.token = token;
    }

    @GetMapping("/getCurrentToken")
    public ApiResults<Map> getCurrentToken(){

        Map<String, Object> currentToken = new HashMap<>();

        currentToken.put("start", token.getStartDate());
        currentToken.put("end", token.getEndDate());
        currentToken.put("totalSupply", token.getTotalsupply());
        currentToken.put("crowdSale", token.getCrowdsale());
        currentToken.put("name", token.getName());
        currentToken.put("symbol", token.getSymbol());
        currentToken.put("decimal", token.getDecimal());
        currentToken.put("price", token.getPrice());
        currentToken.put("token_address", token.getEthContractAddress());
        currentToken.put("bonus", token.getBonusRate());

        return new ApiResults<>("Current Token Details",true,currentToken);
    }
}
