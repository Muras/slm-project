package com.org.controller;


import com.org.Api.ApiResults;
import com.org.model.User;
import com.org.service.UserService;
import com.org.util.CoinsUtil;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/Digi")
public class logInController {

    private UserService userService;
    private BCryptPasswordEncoder passwordEncoder;

    private static final String SIGNINKEY="DIGITOKENSECRsssTKEY105040453";



    public logInController(UserService userService,BCryptPasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @RequestMapping(value = "/logIn/{username}/{password}")
    ApiResults<Map> userCheck(@PathVariable("username") String username, @PathVariable("password") String password) {

        User user = userService.logIn(username);
        //System.out.println(user);


        if (user != null && passwordEncoder.matches(password, user.getPassword())){

            Map<String ,Object> tokenMap = generateToken(user);
            return new ApiResults<Map>("SuccessfullyLoged In",true,tokenMap);
        }

        else
            return new ApiResults<Map>("Incorrect Credentials",false);
    }
    @RequestMapping(value = "/authenticate/token", method = RequestMethod.GET)
    public ApiResults<String> authendicateToken()
    {
        return new ApiResults<String>("valid token",true);
    }

    private Map<String, Object> generateToken(User appUser){

        Map<String, Object> tokenMap = new HashMap<String, Object>();

        Calendar expirationTime = Calendar.getInstance();
        expirationTime.add(Calendar.HOUR_OF_DAY, 10);

       /* long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        long expiration = System.currentTimeMillis()+300000;
        Date expirationTime = new Date(expiration);*/

        String token = Jwts.builder()
                .setSubject(appUser.getUsername())
                .claim("roles", appUser.getRole())
                .claim("email",appUser.getEmail())
                .setIssuedAt(new Date())
                .setExpiration(expirationTime.getTime())
                .signWith(SignatureAlgorithm.HS256, SIGNINKEY).compact();

        tokenMap.put("token", token);
        tokenMap.put("user", appUser);

        return tokenMap;
    }

    @RequestMapping(value="/transaction/{username}/{api_key_label}/{amount}/{fromaddress}")
    ApiResults<Map> transactionBal(@PathVariable("username") String username,@PathVariable("api_key_label") String api,@PathVariable("fromaddress") String fromAddress,@PathVariable("amount") String amount){

         String toAddress = null;

        User user = userService.getUserByusername(username);
        if(api.equals(user.getLite_label())){
            api = "Litecoin.apikey";
           // amount ="0.1";
           // fromAddress = "2N5iv5nboie94pmndDSzMypeMu7uDYsbUkU";
             toAddress =user.getLite_address();
        }
        if(api.equals(user.getBtc_label())){
            api ="Bitcoin.apikey";
           // fromAddress ="2N6j4RNARVDcYZoCvUseLMhnfAu5pzxe8JT";
           // amount ="0.1";
              toAddress =user.getBtc_address();
        }
        if(api.equals(user.getDogi_label())){
            api = "Dogecoin.apikey";
          //  fromAddress = "2N8r1rMT5okNrqKnsjP3uwc51ZtLGjQx1Fa";
           // amount ="0.1";
             toAddress =user.getDogi_address();
        }
        System.out.println(api +" "+fromAddress+" "+toAddress+" "+amount);

        HashMap balaceTransaction = CoinsUtil.withdrawBal(api,fromAddress,toAddress,amount);
        balaceTransaction = (HashMap) balaceTransaction.get("data");

        return new ApiResults<Map>("Trasfered",true,balaceTransaction);

    }

    @RequestMapping(value = "/checkBalance/{username}")
    ApiResults<Map> addressCheck(@PathVariable String username){


        HashMap<String,String> balances = new HashMap();
        User user = userService.getUserByusername(username);

        HashMap checkBalance = CoinsUtil.checkBal("Litecoin.apikey",user.getLite_address());

        checkBalance = (HashMap) checkBalance.get("data");

        //  String Ltcbalance = checkLtcBalance.get("available_balance").toString();
        String Ltcbalance = checkBalance.get("available_balance").toString();
        balances.put("Litecoin",Ltcbalance);
        HashMap checkDogiBalance = CoinsUtil.checkBal("Dogecoin.apikey",user.getDogi_address());

        checkDogiBalance = (HashMap) checkDogiBalance.get("data");

        //  String Ltcbalance = checkLtcBalance.get("available_balance").toString();
        String dogibalance = checkDogiBalance.get("available_balance").toString();
        balances.put("DOgeCoin",dogibalance);

        HashMap checkBtcBalance = CoinsUtil.checkBal("Bitcoin.apikey",user.getBtc_address());

        checkBtcBalance = (HashMap) checkBtcBalance.get("data");

        //  String Ltcbalance = checkLtcBalance.get("available_balance").toString();
        String Btcbalance = checkBtcBalance.get("available_balance").toString();
        balances.put("BitCoin",Btcbalance);

        return new ApiResults<Map>("Balances of Yours",true,balances);
    }

}
