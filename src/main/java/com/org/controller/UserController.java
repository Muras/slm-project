package com.org.controller;

import com.org.Api.ApiResults;
import com.org.util.CoinsUtil;
import com.org.model.User;
import com.org.service.UserService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;


import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/Digi")
public class UserController {


    private UserService userService;
    private BCryptPasswordEncoder passwordEncoder;


    public UserController(UserService userService,BCryptPasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }



    @RequestMapping(value = "/addUser",method = RequestMethod.POST)
    ApiResults<User> registerUser(@RequestBody User user){


        if(user.getUsername().isEmpty() || user.getPassword().isEmpty() || user.getEmail().isEmpty())
            return new ApiResults<User>("SomeField is empty",false);


        else if(userService.checkExistUsername(user.getUsername()) == false)
            return new ApiResults<User>("Username Exists",false);


        else if(userService.checkExistemail(user.getEmail()) == false)
            return new ApiResults<User>("Email Exists",false);


        if(user.getUsername().equals("Murasu") && user.getPassword().equals("password")){

            user.setRole("ADMIN");
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userService.addUser(user);
            return new ApiResults<User>("Admin Role Created",true);

        }

        else {

            // ** For Lite Coin
            HashMap ltcResult = CoinsUtil.generateWallet("Litecoin.apikey");



            if(!ltcResult.get("status").equals("success"))
                return new ApiResults<User>("LiteCoin failed",false);

            HashMap litcoinAddress = (HashMap) ltcResult.get("data");

            user.setLite_address(litcoinAddress.get("address").toString());

            user.setLite_label(litcoinAddress.get("label").toString());

            // ** For DogeCoin
            HashMap DogeResult = CoinsUtil.generateWallet("Dogecoin.apikey");


            if(!DogeResult.get("status").equals("success"))
                return new ApiResults<User>("DogeCoin failed",false);

            HashMap DogecoinAddress = (HashMap) DogeResult.get("data");

            user.setDogi_address(DogecoinAddress.get("address").toString());

            user.setDogi_label(DogecoinAddress.get("label").toString());

            // ** For Bit Coin
            HashMap BitResult = CoinsUtil.generateWallet("Bitcoin.apikey");


            if(!BitResult.get("status").equals("success"))
                return new ApiResults<User>("Bit failed",false);

            HashMap BitCoinDetails = (HashMap) BitResult.get("data");

            user.setBtc_address(BitCoinDetails.get("address").toString());

            user.setBtc_label(BitCoinDetails.get("label").toString());
            user.setRole("USER");

            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userService.addUser(user);
            return new ApiResults<User>("SuccessFully Added ",true,user);
        }
    }

    @GetMapping("/admin/allUser")
    ApiResults<List> allUser(){

        List<User> userList = userService.allUser();
        //System.out.println(userList);
        return new ApiResults<List>("All users",true,userList);

    }
    @RequestMapping("/getSingleUser/{id}")
    ApiResults<User> getSingleUser(@PathVariable("id") Long id){
        System.out.println("Here");
        User user = userService.getUserById(id);
        return new ApiResults<User>("User Is ", true,user);
    }
    @RequestMapping ("/change_password/{username}/{currentpass}/{newpass}")
    public ApiResults<String> changePassword(@PathVariable("username") String username,
                                            @PathVariable("currentpass") String currentpass,
                                            @PathVariable("newpass") String newpass)
    {

        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(currentpass) || StringUtils.isEmpty(newpass))
            return new ApiResults<String>("Invalid",false);


        User user = userService.logIn(username);

        if(!passwordEncoder.matches(currentpass, user.getPassword()))
            return new ApiResults<String>("Invalid",false);
        user.setPassword(passwordEncoder.encode(newpass));

        userService.addUser(user);

        return new ApiResults<String>("Password Changed Successfully",true);
    }


}
