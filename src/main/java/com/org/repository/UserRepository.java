package com.org.repository;

import com.org.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public interface UserRepository  extends JpaRepository<User,Long>{

    User findByusername(String name);
    User findByid(Long id);

    @Query("Select count(u)=0 from User u where u.username = ?1 ")
    boolean checkExistsUsername(String username);
    @Query("Select count(u)=0 from User u where u.email = ?1 ")
    boolean checkExistsemail(String email);
}
