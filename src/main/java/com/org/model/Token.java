package com.org.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by thomson on 4/10/17.
 */

@Component
@PropertySource("classpath:token.properties")
@ConfigurationProperties(prefix="token")
public class Token {


    private String name;

    private String symbol;

    private Integer totalsupply;

    private Integer crowdsale;

    private Double price;

    private Integer decimal;

    private String startDate;

    private String endDate;

    private String ltcOwnerAddress;

    private String dogOwnerAddress;

    private String btcOwnerAddress;

    private String ethContractAddress;

    private String ethOwnerAddress;

    private String ethOwnerAddressPrivateKey;

    private Map<String, String> bonusRate;

    public Token() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Integer getTotalsupply() {
        return totalsupply;
    }

    public void setTotalsupply(Integer totalsupply) {
        this.totalsupply = totalsupply;
    }

    public Integer getCrowdsale() {
        return crowdsale;
    }

    public void setCrowdsale(Integer crowdsale) {
        this.crowdsale = crowdsale;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getDecimal() {
        return decimal;
    }

    public void setDecimal(Integer decimal) {
        this.decimal = decimal;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEthContractAddress() {
        return ethContractAddress;
    }

    public void setEthContractAddress(String ethContractAddress) {
        this.ethContractAddress = ethContractAddress;
    }

    public Map<String, String> getBonusRate() {
        return bonusRate;
    }

    public void setBonusRate(Map<String, String> bonusRate) {
        this.bonusRate = bonusRate;
    }

    public String getLtcOwnerAddress() {
        return ltcOwnerAddress;
    }

    public void setLtcOwnerAddress(String ltcOwnerAddress) {
        this.ltcOwnerAddress = ltcOwnerAddress;
    }

    public String getDogOwnerAddress() {
        return dogOwnerAddress;
    }

    public void setDogOwnerAddress(String dogOwnerAddress) {
        this.dogOwnerAddress = dogOwnerAddress;
    }

    public String getBtcOwnerAddress() {
        return btcOwnerAddress;
    }

    public void setBtcOwnerAddress(String btcOwnerAddress) {
        this.btcOwnerAddress = btcOwnerAddress;
    }

    public String getEthOwnerAddress() {
        return ethOwnerAddress;
    }

    public void setEthOwnerAddress(String ethOwnerAddress) {
        this.ethOwnerAddress = ethOwnerAddress;
    }

    public String getEthOwnerAddressPrivateKey() {
        return ethOwnerAddressPrivateKey;
    }

    public void setEthOwnerAddressPrivateKey(String ethOwnerAddressPrivateKey) {
        this.ethOwnerAddressPrivateKey = ethOwnerAddressPrivateKey;
    }
}
