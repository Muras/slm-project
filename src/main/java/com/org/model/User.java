package com.org.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "User")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    public Long id;
    @Column(name = "UserName")
    public  String username;
    @Column(name = "Password")
    public String password;
    @NotNull
    @NotEmpty
    @Email
    @Column(name = "Email")
    private String email;
    @Column(name = "btc_address")
    private String btc_address;
    @Column(name = "lite_address")
    private String lite_address;
    @Column(name = "dogi_address")
    private String dogi_address;
    @Column(name = "btc_label")
    private String btc_label;
    @Column(name = "lite_label")
    private String lite_label;
    @Column(name = "dogi_label")
    private String dogi_label;
    @Column(name = "role")
    private String role;
    public User()
    {

    }

    public User(Long id, String username, String password, String email, String btc_address, String lite_address, String dogi_address, String btc_label, String lite_label, String dogi_label, String role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.btc_address = btc_address;
        this.lite_address = lite_address;
        this.dogi_address = dogi_address;
        this.btc_label = btc_label;
        this.lite_label = lite_label;
        this.dogi_label = dogi_label;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBtc_address() {
        return btc_address;
    }

    public void setBtc_address(String btc_address) {
        this.btc_address = btc_address;
    }

    public String getLite_address() {
        return lite_address;
    }

    public void setLite_address(String lite_address) {
        this.lite_address = lite_address;
    }

    public String getDogi_address() {
        return dogi_address;
    }

    public void setDogi_address(String dogi_address) {
        this.dogi_address = dogi_address;
    }

    public String getBtc_label() {
        return btc_label;
    }

    public void setBtc_label(String btc_label) {
        this.btc_label = btc_label;
    }

    public String getLite_label() {
        return lite_label;
    }

    public void setLite_label(String lite_label) {
        this.lite_label = lite_label;
    }

    public String getDogi_label() {
        return dogi_label;
    }

    public void setDogi_label(String dogi_label) {
        this.dogi_label = dogi_label;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
